# Administrative areas

## Selected Datasets

| Name                          | Description             | Source                                                               | Type   | Format              | Granularity                                                 | License                                       | Comment                                                |
| ----------------------------- | ----------------------- | -------------------------------------------------------------------- | ------ | ------------------- | ----------------------------------------------------------- | --------------------------------------------- | ------------------------------------------------------ |
| [GADM](/GADM)                 | Administrative areas    | [GADM data](https://gadm.org/download_country_v3.html) (version 3.6) | Vector | Shapefiles          | Country (0)<br>Région (1)<br>Département (2)<br>Commune (3) | [GADM license](https://gadm.org/license.html) | [Metadata description](https://gadm.org/metadata.html) |
| [Country](/Country)           | Country boundaries      | OpenStreetMap                                                        | Vector | GeoJSON, Shapefiles | Country (0, OSM `admin_level=2`)                            | ODbL                                          |                                                        |
| [Regions](/Regions)           | Regions boundaries      | OpenStreetMap                                                        | Vector | GeoJSON, Shapefiles | Region (1, OSM `admin_level=2`)                             | ODbL                                          |                                                        |
| [Departements](/Departements) | Departements boundaries | OpenStreetMap                                                        | Vector | GeoJSON, Shapefiles | Departement (2, OSM `admin_level=6`)                        | ODbL                                          |                                                        |

## Regions

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Provinces_of_Cameroon_numbered.svg/457px-Provinces_of_Cameroon_numbered.svg.png)

## Table

|     | Region             |  
| --- | ------------------ |
| 1   | Adamawa            |
| 2   | Central Cameroon   |
| 3   | East Cameroon      |
| 4   | Far North Cameroon |
| 5   | Littoral Cameroon  |
| 6   | North Cameroon     |
| 7   | Northwest Cameroon |
| 8   | South Cameroon     |
| 9   | Southwest Cameroon |
| 10  | West Cameroon      |
